# Gerrit User Summit 2022 - Schedule

## Hackathon - 7,8 and 9 November

Three days of hacking on Gerrit Code Review and for building new plugins
by contributors and people passionate about understanding and extending
Gerrit.

The hackathon will take place at [Spaces Aurora](https://g.page/71-Uxbridge-Road-5099?share),
in the historical borough of [Ealing](https://en.wikipedia.org/wiki/London_Borough_of_Ealing),
well known for being the birthplace of [Ada Lovelace](https://en.wikipedia.org/wiki/Ada_Lovelace),
the very first software developer in history.

**Schedule**

All the three days will follow this schedule:

| Time  | Session
|-------|----------------------------
|  9:00 | Welcome to the Hackathon
|  9:30 | Coffee and breakfast nearby
| 10:00 | _Hacking together_
| 12:00 | Lunch & Networking
| 13:00 | _Hacking together_
| 18:00 | End of the day

Remote contributors and maintainers will be able to interact remotely
using the [Discord Event](https://discord.gg/n6KJmZUs?event=1027503668041363517)
and [Discord Channel](https://discord.com/channels/775374026587373568/1027206237957410887).

*** note
__By invitation only__

The number of places available is very limited.
The primary purpose of the hackathon is to speed up the communication
between Gerrit contributors with white-boarding face-to-face sessions
and pair programming in a quiet and co-located roundtable.

The contributors and maintainers attending the hackathon will also
help with the release of the forthcoming Gerrit v3.7.0.
***

## Gerrit User Summit, 10-11 November

Introduction, intermediate and advanced sessions on Gerrit Code Review.

### Thursday, 10th of November

| Time  | Session
|-------|----------------------------------------------------------------
|  9:00 | Registration Opens, Breakfast & Networking
|  9:30 | Welcome introduction
|  9:45 | [What's new in Gerrit 3.6 and 3.7](sessions/whats-new-gerrit-3.6-3.7.md)
| 10:30 | [Wrestling large repos with JGit an Gerrit](sessions/wrestling-large-repos.md)
| 11:15 | [Q&A with the Gerrit Maintainers](sessions/maintainers-qa.md)
| 12:00 | Lunch & Networking
| 14:00 | [Gerrit for smoothly switching from SVN to Git](sessions/svn-to-git.md)
| 14:45 | [Gerrit adoption and NoteDb migration user story](sessions/gerrit-user-story.md)
| 15:30 | Break & Networking
| 16:00 | [Using Gerrit with Zuul](sessions/gerrit-and-zuul.md)
| 16:45 | [How healthcheck and multi-site saves your sleep at night](sessions/healthcheck-multi-site-keep-sleep-at-night.md)
| 17:30 | Party and free drinks

### Friday, 11th of November

| Time  | Session
|-------|-------------------------------------------
|  9:00 | Breakfast & Networking
|  9:30 | [Import Gerrit projects across servers](sessions/import-gerrit-projects-across-servers.md)
| 10:15 | [What's new in the UI](sessions/whats-new-ui.md)
| 11:00 | [What's new in pull-replication plugin](sessions/whats-new-in-pull-replication.md)
| 12:00 | Lunch& Networking
| 14:00 | [Keeping an eye on your repository metrics](sessions/git-repo-metrics.md)
| 14:10 | [Status update of the "Gerrit on Kubernetes" project](sessions/k8s-gerrit.md)
| 14:20 | [Submit requirements - Goodbye Prolog](sessions/submit-requirements.md)
| 14:30 | [What's new in the OWNERS plugin](sessions/owners-plugin.md)
| 14:40 | ["checks-jenkins" - A Checks-API implementation for Jenkins](sessions/checks-jenkins.md)
| 14:50 | __Lightning talk available__
| 15:00 | Break & Networking
| 15:30 | Final retrospective and proposals
| 16:30 | Conference wrap-up and Closing Keynote
