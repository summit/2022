# Gerrit User Summit is *THE event of the year* for the Community.

We will bring together Gerrit thought leaders, developers, practitioners,
the whole community and ecosystem, in one event, providing attendees
with the opportunity to learn, explore, network face-to-face and help
shaping the future of Gerrit development and solutions.

# Gerrit User Summit 2022 - 7-11 November, London (UK)

| 7,8,9 November     | 10,11 November    |
|--------------------|-------------------|
| 3 days Hackathon   | 2 days Conference |

# Back face-to-face after 2 years of remote events

After two years of remote meetings and virtual conferences, this year,
we are back face-to-face in the heart of the vibrant City of London.

Similarly to what we have experimented in [Sunnyvale in 2019](https://gerrit.googlesource.com/summit/2019/+/master/schedule-usa.md),
the event will also be broadcasted on a live stream to allow people
to join from around the globe.

# Celebrate the comeback of the Summit, drinks are on us

To celebrate the return to a face-to-face Gerrit User Summit, we have organised
free drinks on the 10th of November, from 5 PM till the evening.
Feel free to come and network with the other members of the community,
all drinks will be on us.

# Engaging remotely using Discord

For the first time in the history of the Gerrit User Summits, we will also
have a parallel platform to encourage peer-to-peer interaction during the
meeting and beyond.

The Gerrit community has moved to [Discord](https://discord.gg/HkGbBJHYbY) and
we will encourage people to continue their discussions, participate remotely
and engage with the attendees and speakers in London.