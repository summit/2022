# Gerrit User Summit 2022 - Location

## CodeNode

The User Summit venue is [CodeNode](https://codenode.live)
in the lower-ground floor dedicated to meetups and conferences.

CodeNode is a 23,000 sqft Tech Events and Community venue. CodeNode provides fantastic meetup,
conference, training and collaboration spaces with unique technology capabilities for our tech,
digital and developer communities.

CodeNode features a 5,000 sqft break-out space, complete with fully-licensed bar, plenty of power sockets,
meeting and collaboration spaces and entertainment areas.

## Hotels and Places to Stay

There are plenty of [Hotels near Liverpool Street](https://goo.gl/maps/yAZEst1kfgHuL4ea8) which is
walking distance form the venue of the event.

Many of the Gerrit contributors and maintainers are staying at the [YOTEL London Shoreditch](https://www.yotel.com/en/hotels/yotel-london-shoreditch)
if you would like to have interesting networking opportunities in the evenings or early mornings.

## Getting there

**By Car**

If you want to commute by car to CodeNode, NCP Finsbury Square offers an ideal parking spot.
Located beneath the lawn bowls green you're only a short walk from CodeNode, Moorgate and Old Street.

You can also check out [Parkopedia](https://en.parkopedia.co.uk/parking/locations/south_place_city_of_london_london_ec2m_2up_united_kingdom_cg3egcpvn48ze4z85i/?country=uk&arriving=202211100830&leaving=202211111700)
for details of nearby parking.

*** note
__ULEZ and Congestion Charge__

Bear in mind that you need to have an electric or other zero-emission vehicle for avoiding
charges when entering the _Ultra Low Emission Zone_ (ULEZ). You also need to pay extra for entering
the central London zone with a car within the _Congestion Charge_ area.

Refer to to the [TFL Congestion Charge](https://tfl.gov.uk/modes/driving/congestion-charge) site for more details.

***


**By Bicycle**

For cycling directions, check out a bike map around CodeNode.

During the event, you can store your bicycle at the reception area, but please let one of our staff know on arrival.
Please note that your bike will be left at your own risk.

**By Train**

The nearest Underground station is Liverpool Street, using the [Elisabeth Line](https://www.crossrail.co.uk/route/stations/liverpool-street/),
Circle, Metropolitan, Hammersmith & City, Northern line and the Overground.

There is a [3 minutes walk from Liverpool Street Station](https://goo.gl/maps/1QeFKYfPzgbnZrfaA) to 10 South Place.

## Inside CodeNode, lower-ground floor map

See below the map of the CodeNode lower ground floor where the User Summit will take place.
There will be roll-up banners at the entrance of CodeNode and in front of the Conference Room used for the two days event.

![CodeNode - Gerrit User Summit](images/codenode-lowerground.png)
