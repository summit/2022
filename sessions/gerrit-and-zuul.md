# Using Gerrit with Zuul

Zuul is a project gating system that goes beyond any other CI or CD system.

Originally developed exclusively for Gerrit and OpenStack, it has
grown the ability to interact with many other code review systems and
clouds.  But Gerrit holds a special place in Zuul: there are some
amazing workflows that are only possible when using Gerrit and Zuul
together.

This presentation will focus on using Gerrit and Zuul effectively, including:

- Cross repo testing (you don't have to use a monorepo!)
- Effective testing with Gerrit's Git submodule tracking
- Simultaneous testing and merging of cross repo dependencies
- Testing Gerrit and non-Gerrit repos together

*[James Blair, Acme Gating](../speakers.md#corvus)*
