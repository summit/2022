# Status update of the "Gerrit on Kubernetes" project

The "Gerrit on Kubernetes" (short: k8s-gerrit) project aims to provide a simple
way of deploying Gerrit into a Kubernetes cluster.

At SAP we are planning to move our Gerrit instances to Kubernetes clusters in
the upcoming year. To make this possible, we are working on a Kubernetes operator
to manage Gerrit deployments and on making Gerrit scalable using the multisite
plugin.

Thomas will present a short overview over the current status and the upcoming
plans.

This talk will be presented online.

*[Thomas Draebing, SAP](../speakers.md#tdraebing)*
