# Q&A with the Gerrit maintainers

Meet the Gerrit maintainers and ask those questions that
you've always been wanting to ask.

* *[Luca Milanesio, GerritForge](../speakers.md#lmilanesio)*
* *[Edwin Kempin, Google](../speakers.md#ekempin)*
* *[Ben Rohlfs, Google](../speakers.md#brohlfs)*
* *[Thomas Draebing, SAP](../speakers.md#tdraebing)*
* *[Patrick Hiesel, Google](../speakers.md#phiesel)*
* *[Matthias Sohn, SAP](../speakers.md#msohn)*
