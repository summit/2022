# Submit Requirements - Goodbye Prolog

Submit requirements is the new and simpler way with which admins can configure
submittability rules for their projects, that is, rules that should be satisfied
before a change can be submittable.

Historically, Gerrit had three different ways to define change submittability:
label functions, custom submit rules and prolog. The new submit requirement is
meant to replace them all. The major Google owned gerrit hosts have fully
migrated to submit requirements including Gerrit, all Chromium and Android among
others. All other hosts are expected to be fully migrated within the next three
months.

Youssef will present an overview of submit requirements, how they can be
configured by host admins and how they are displayed in the change page and the
dashboard.

This talk will be presented online.

*[Luca Milanesio, GerritForge](../speakers.md#lmilanesio)*