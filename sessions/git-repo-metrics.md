# Keeping an eye on your repository size

Maintaining a monorepo comes with its challanges, one of which
is monitoring the evolution of the repository.

Fabio will talk about a [new plugin](https://gerrit.googlesource.com/plugins/git-repo-metrics)
to systematically collect metrics about a git repository.
This is crucial in particular when dealing with a monorepo.
The new metrics allow, for example, planning of GC cycles,
alerting when a GC is not effective or help with the capacity
planning of the hardware.

*[Fabio Ponciroli, GerritForge](../speakers.md#fponciroli)*
