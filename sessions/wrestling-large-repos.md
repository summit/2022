# Wrestling large repos with JGit and Gerrit

GerritForge has been focusing in 2022 on improving the scalability and performance of Gerrit with large repositories.

Luca presents an overview of the significant findings, some of them surprising and scary at the same time, including:
- The speed of SHA1 with packed refs
- Impact of NFS and file/folder stats
- JGit connectivity checks
- Negotiations black holes
... and **more to be revealed** during the talk!

*[Luca Milanesio, GerritForge](../speakers.md#lmilanesio)*
