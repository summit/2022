# What's new in the pull-replication

Over the last two year GerritForge took an effort to improve Gerrit replication
performance by introducing the pull-replication plugin.

In his talk Marcin and Alvaro will focus on the improvements and the new features introduced
to the pull-replication plugin:
* E2E metrics
* Use events-broker and stream events as a backfill mechanism
* A new apply-objects REST-API
* JGit performance improvements
* Bearer Token Authentication
* And many other small improvemnents


*[Marcin Czech, GerritForge](../speakers.md#mczech)*
*[Alvaro Vilaplana Garcia, GerritForge](../speakers.md#avilaplana)*
