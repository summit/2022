# What's new in the UI?

Frank and Ben are part of Google's developer team that works on Gerrit's web
application. They will walk you through all the changes and enhancements that
were done for the past two releases.

This talk will be presented online.

*[Frank Borden, Google](../speakers.md#frankborden)*
*[Ben Rohlfs, Google](../speakers.md#brohlfs)*
