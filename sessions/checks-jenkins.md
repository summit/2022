# "checks-jenkins" - A Checks-API implementation for Jenkins

The [Checks-API](https://gerrit-review.googlesource.com/Documentation/pg-plugin-checks-api.html)
was recently added to Gerrit to provide an extension point for plugins that
collect data from CI systems to display them in the Gerrit UI.

The [checks-jenkins plugin](https://gerrit-review.googlesource.com/admin/repos/plugins/checks-jenkins,general)
implements the Checks-API for Jenkins CI servers. It relies on the
[gerritchangequery plugin](https://review.gerrithub.io/admin/repos/tdraebing/gerritchangequery-plugin,general)
to query job runs working on a patchset in Jenkins and displays the collected
data in the Gerrit UI. The gerritchangequery plugin supports the gerrit trigger
plugin and the gerritcodereview plugin.

The presented plugins are still work-in-progress and created during the last
two Gerrit hackathons.

This talk will be presented online.

*[Thomas Draebing, SAP](../speakers.md#tdraebing)*
