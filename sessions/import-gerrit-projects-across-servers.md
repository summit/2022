# Import Gerrit projects across servers

NoteDb was introduced 5 years ago with the promise of
simpifying the portability of reviews and repositories across
Gerrit servers and, in the future, different code-review systems.
The promise was that a JSON-based representation of the review
meta-data would have allowed to rebuild a full state and history
anywhere, without having to rely on a centralised DBMS.

With that promise in mind, the development of the [Gerrit importer plugin](https://gerrit.googlesource.com/plugins/importer/)
developed by [SAP](https://sap.com) stopped at version v2.15 and
no more upgrades where planned.

From Gerrit v2.16 up to now, the Gerrit admins were unable to
follow any phased-migration as they could have done with the
use of the importer plugin, forcing companies to go through a
painful release-by-release upgrade process or multi-year bing-bang
migration projects.

Luca gives an overview of how the migration and consolidation
is now possible with Gerrit v3.7, describing the challenges and
limitations of what can be done.

- [Slides](https://storage.googleapis.com/gerrit-talks/summit/2022/Import%20Gerrit%20projects%20across%20servers.pdf)
- [Video](https://youtu.be/Su1OpJ_s850)

*[Luca Milanesio, GerritForge](../speakers.md#lmilanesio)*
