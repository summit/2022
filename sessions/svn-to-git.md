# Gerrit for smoothly switching from a Subversion poly-repo to a Git mono-repo

Many corporate environments still use Subversion (SVN) for version
control. Switching to Git can be a disruptive step that's often
postponed.

In this session, an approach will be presented that has been taken for
switching a SVN-based development environment to a Git/Gerrit workflow
in a less disruptive and flexible way. The approach consists of

1. migrating a large number of interconnected SVN repositories to a
   Git mono-repo, and

2. establishing a birectional hot-sync between Git and SVN, allowing
   to concurrently use both Git and SVN.

The implemented hot-sync allows to flexibly switch to Git, and the
change-based Gerrit workflow was an enabler for it.

The presentation will cover key concepts of the migration; it also
provides a summary of the modifications to Gerrit and the hooks plugin
for supporting the hot-sync setup.

*[Alexander Ost, Freelance Consultant](../speakers.md#aost)*
