# What's new in Gerrit v3.6 and v3.7

Gerrit v3.6 and v3.7 have been released in Spring and Autumn 2022.

Luca gives an overview of the major improvements introduced:

- Goodbye Prolog, welcome Submit Requirements
- Markdown support
- Mention in reviews

*[Luca Milanesio, GerritForge](../speakers.md#lmilanesio)*
