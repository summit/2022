# How we adopted and evolved Gerrit in the past 6 years

The introduction and adoption of Gerrit in a large enterprise
like NOKIA comes with challenges and opportunities.

Jose will present how it all started back in 2015 from Alcatel-Lucent and Gerrit
evolved to an enterprise-level setup with DR-size used by over 15k developers
worldwide.

Our journey is not over as we are heading to NoteDb and
further: I would share with you the hurdles we encountered in our upgrade
process how we plan to move forward to make our setup
more scalable and up-to-date with the latest and greatest
version of Gerrit.

*[José Granha - NOKIA](../speakers.md#jgranha)*
