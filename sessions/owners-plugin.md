# What's new in the OWNERS plugin

Some new feature have been introduced in the OWNERS plugin in the last months.

Fabio will talk about some REST APIs and OWNERS file logic inheritance
that have been added to the [plugin](https://gerrit.googlesource.com/plugins/owners/)
lately.

*[Fabio Ponciroli, GerritForge](../speakers.md#fponciroli)*