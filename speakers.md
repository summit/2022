# Gerrit User Summit 2022 - Speakers

### Luca Milanesio - GerritForge {#lmilanesio}

[LinkedIn](https://www.linkedin.com/in/lucamilanesio/)

Luca is co-founder of GerritForge and has over 30 years of software development
and application lifecycle management experience.
He has been Gerrit Contributor since 2012, Gerrit Release manager,
member of the Gerrit Engineering Steering Committee and maintainer
of [GerritHub.io](https://gerrithub.io), the Open Service for Gerrit Code Review
on top of GitHub repositories.

### James Blair - Acme Gating {#corvus}

[LinkedIn](https://www.linkedin.com/in/jamesblair2/)

James is the project lead for the Zuul project gating system, and a
founding member of the OpenDev collaboratory team.  As a sysadmin and
hacker he gets to write elegant code and then try to make it work in the
real world.  He started [Acme Gating](https://acmegating.com/) to provide
enterprise Zuul support and services.

### Fabio Ponciroli - GerritForge {#fponciroli}

[LinkedIn](https://uk.linkedin.com/in/fponciroli/)

Fabio is a Senior Software Engineer at GerritForge, where he contributes to the
OpenSource Gerrit Code Review project.
He has been involved since the beginning in the design and development of Gerrit
[DevOps analytics](https://gerrit.googlesource.com/plugins/analytics/)
tools, [multi-site](https://gerrit.googlesource.com/plugins/multi-site/) plugin
and many other.
He has also created the [Gatling protocol manager for Git](https://github.com/GerritForge/gatling-git)
which has finally provided a real E2E test suite for the validation of
the Gerrit Code Review releases.
He has extensive experience in working on backend systems, on-premise and
cloud-based, with different programming languages, such as Scala, Java, NodeJS
and related ecosystems.

### Thomas Draebing - SAP {#tdraebing}

[LinkedIn](https://www.linkedin.com/in/thomasdraebing/)

Thomas is a Software Developer at SAP, where he is part of the team providing
Gerrit for development teams within SAP. He is actively contributing to Gerrit
since 2018 and has started the [k8s-gerrit](https://gerrit-review.googlesource.com/admin/repos/k8s-gerrit,general)
project.

### Alexander Ost - Freelance Consultant {#aost}

[LinkedIn](https://www.linkedin.com/in/alexander--ost/)

Alexander has been working as a CI/SCM/DevOps consultant since
2004. He has been working with Gerrit for various companies, and he
has obtained experience in using, customizing and troubleshooting
Gerrit from several large-scale projects.

### Frank Borden - Google {#frankborden}

[LinkedIn](https://www.linkedin.com/in/frank-borden-1ba62099/)

Frank is a software engineer working on Gerrit's web application.
He joined Gerrit in 2020.

### Ben Rohlfs - Google {#brohlfs}

[LinkedIn](https://www.linkedin.com/in/ben-rohlfs-4b7778178/)

Ben is a software engineer working on Gerrit's web application.
He joined Gerrit in 2019, and has been working on the Attention
Set and the Check UI.

### Marcin Czech - GerritForge {#mczech}

Marcin is a Senior Software Engineer at GerritForge where he contributes to the
OpenSource Gerrit Code Review project.

Over the last three years he has been involved in the design and development of
numerous parts of the Gerrit ecosystem, such as: Gerrit core, multi-site,
events-broker(Kafka and AWS Kinesis) and pull-replication.

### Youssef Elghareeb - Google {#ghareeb}

[LinkedIn](https://www.linkedin.com/in/youssef-elghareeb/)

Youssef is a Software Engineer at Google since Nov. 2019 and a maintainer of the
Gerrit open source project. His major contributions to the Gerrit project
include the redesign of Gerrit's diff cache system and revamping the legacy
submittability rules in favour of submit requirements.

### Alvaro Vilaplana Garcia - GerritForge {#avilaplana}

[LinkedIn](https://www.linkedin.com/in/avilaplana2008/)

Alvaro is a Senior Software Engineer at GerritForge where he contributes to the
OpenSource Gerrit Code Review project.

Over the last months he has been involved in the design and development of
the Bearer Token Authentication in pull-replication.

### Edwin Kempin - Google {#ekempin}

Edwin is a long-time Gerrit contributor. He joined the Gerrit project in 2010,
became a maintainer in 2012 and assumed the role as community manager in 2019.

Since 2015 he's working at Google in the Gerrit backend team that develops Gerrit
core and maintains the Gerrit infrastructure at Google.
Previously Edwin worked 10+ years at SAP where he was responsible for Gerrit
development and hosting as well.

### José Granha - NOKIA {#jgranha}

[LinkedIn](https://www.linkedin.com/in/jos%C3%A9-granha-3496841/)

José is member of the IT R&D SCM Center of Excellence, located in Lisboa, PT.
He started in 2019 as a Solution designer for the Gerrit R&D ecosystem in Nokia
and later became project manager for the SCM architecture transformation and
enhancement of the company.

### Patrick Hiesel - Google {#phiesel}

Patrick joined Google and the Gerrit project in 2016, working primarily on the
backend. Since then, he has worked on making Gerrit faster, and more stable as
well as different features, including (most recently) Submit Requirements.

He is a Gerrit maintainer and a member of the Engineering Steering Committee.

### Antonio Barone - GerritForge {#abarone}

[LinkedIn](https://uk.linkedin.com/in/anbarone/)

Antonio joined GerritForge in 2018. Since then, he has contributed to the design
and development of various tools and plugins for Gerrit, such as DevOps
analytics, multi-site and chronicle-map. Recently, he heroed in on the
integration of Gerrit with the cloud.

### Matthias Sohn - SAP {#msohn}

Matthias is a long-time Gerrit contributor, maintainer and community manager.
He leads the Gerrit team at SAP and the JGit and EGit projects at Eclipse.
