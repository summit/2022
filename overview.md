# What is Gerrit Code Review?

Gerrit provides web based code review and repository management
for the Git version control system.

If you are new to Gerrit, you may read a [Quick Introduction](https://gerrit-review.googlesource.com/Documentation/intro-quick.html)
directly from the Google-hosted documentation Gerrit web-site:

> Gerrit is intended to provide a lightweight framework for reviewing
> every commit before it is accepted into the code base. Changes are
> uploaded to Gerrit but don’t actually become a part of the project
> until they’ve been reviewed and accepted.

# What is Gerrit User Summit?

Gerrit User Summit and Hackathon is THE event for everything related
to the Gerrit Code Review Community.

The Gerrit User Summit is the place where all major Gerrit users
annually meet and exchange experiences and practices that foster the
community with new and useful ideas.

## Gerrit Community in Numbers

* 14 years of activity
* 20 events in USA, UK and Germany
* 1000+ contributors worldwide
* 292 releases
* 160+ plugins
* 6,500+ topics discussed on the repo-discuss mailing list 20,000 users registered on GerritHub.io
* 1500+ teams on GerritHub.io